from kivy.app import App
from kivy.core.window import Window

Window.size = (400, 660)
Window.title = 'Калькулятор'

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button

class CalcApp(App):

    def on_button_press(self,instance):
        if instance.text == "C":
            self.text_input.text = ""
        else:
            self.text_input.text += instance.text

    def sqr_on_press(self, *args):
        if self.text_input.text:
            x = int(self.text_input.text) ** 2
            self.text_input.text = str(x)
                
    def sqrt_on_press(self, *args):
        if self.text_input.text:
            x = int(self.text_input.text) ** 0.5
            self.text_input.text = str(x)
           
    def bin_on_press(self, *args):
        if self.text_input.text:
            x = format(int(self.text_input.text),'b')
            self.text_input.text = str(x)
            
    def dec_on_press(self, *args):
        if self.text_input.text:
            x = int(self.text_input.text, 2)
            self.text_input.text = str(x)

    def percent_on_press(self, *args):
        if self.text_input.text:
            x = int(eval(self.text_input.text)) / 100
            self.text_input.text = str(x)
    
    def equals_on_press(self, *args):
        if self.text_input.text:
            try:
                x = int(eval(self.text_input.text))
                self.text_input.text = str(x)
            except:
                self.text_input.text = 'Error'
    
    def build(self):
        main_layout = BoxLayout(orientation = "vertical", padding = 15)
        
        self.text_input = TextInput(multiline = False, readonly = False, halign ='right',
                                    font_size = 140, input_filter = 'float',
                                    pos_hint = {"center_x" : 0.5, "center_y": 0.5})
        
        main_layout.add_widget(self.text_input)
        
        f_top_layout = BoxLayout()

        sqr_button = Button(text = "sqr", pos_hint = {"center_x" : 0.5, "center_y": 0.5}, font_size = 40)
        sqr_button.bind(on_press=self.sqr_on_press)
        sqrt_button = Button(text = "sqrt", pos_hint = {"center_x" : 0.5, "center_y": 0.5}, font_size = 40)
        sqrt_button.bind(on_press=self.sqrt_on_press)
        bin_button = Button(text = "bin", pos_hint = {"center_x" : 0.5, "center_y": 0.5}, font_size = 40)
        bin_button.bind(on_press=self.bin_on_press)
        dec_button = Button(text = "dec", pos_hint = {"center_x" : 0.5, "center_y": 0.5}, font_size = 40)
        dec_button.bind(on_press=self.dec_on_press)
        
        f_top_layout.add_widget(sqr_button)
        f_top_layout.add_widget(sqrt_button)
        f_top_layout.add_widget(bin_button)
        f_top_layout.add_widget(dec_button)

        main_layout.add_widget(f_top_layout)

        s_top_layout = BoxLayout()
        opener_button = Button(text = "(", pos_hint = {"center_x" : 0.5, "center_y": 0.5},font_size = 40)
        opener_button.bind(on_press=self.on_button_press)
        closer_button = Button(text = ")", pos_hint = {"center_x" : 0.5, "center_y": 0.5},font_size = 40)
        closer_button.bind(on_press=self.on_button_press)
        percent_button = Button(text = "%", pos_hint = {"center_x" : 0.5, "center_y": 0.5},font_size = 40)
        percent_button.bind(on_press=self.percent_on_press)
        equals_button = Button(text = "=", pos_hint = {"center_x" : 0.5, "center_y": 0.5},font_size = 40)
        equals_button.bind(on_press=self.equals_on_press)
        
        s_top_layout.add_widget(opener_button)
        s_top_layout.add_widget(closer_button)
        s_top_layout.add_widget(percent_button)
        s_top_layout.add_widget(equals_button)
        
        main_layout.add_widget(s_top_layout)
        
        but_list = [
            ["7","8","9","+"],
            ["4","5","6","-"],
            ["1","2","3","*"],
            [".","0","C","/"],
        ]
        for row in but_list:
            h_layout = BoxLayout()
            for button in row:
                button_instance = Button(text = button, pos_hint = {"center_x" : 0.5, "center_y": 0.5}, font_size = 40)
                button_instance.bind(on_press = self.on_button_press)
                h_layout.add_widget(button_instance)
                
            main_layout.add_widget(h_layout)
        return main_layout

if __name__ == '__main__':
    CalcApp().run()