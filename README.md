# Знакомство с Kivy

1, Для того что бы ваше приложение запускалось необходимо ввести этот код:

```python
class PongApp(App):
    def build(self):
        game = PongGame()
        game.serve_ball()
        Clock.schedule_interval(game.update, 1.0 / 60.0)
        return game


if __name__ == '__main__':
    PongApp().run()
```
