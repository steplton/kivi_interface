# Перевод из двоичной в десятичную
def from_duo_to_dec(input_cifer):
    x = 0
    s = 0

    input_list = list(input_cifer)
    int_list = [int(elems) for elems in input_list]
    int_list = int_list[::-1]
    
    for i in range(len(int_list)):
        if int_list[i] == 1:
            x = 2**i
            s+=x
    return s
# Перевод из десятичной в двоичную
def from_dec_to_duo():
    
    print("Введите десятичное число:")

    input_cifer = int(input())
    duo_list = []
    main = input_cifer // 2
    
    while main !=0:
        lost = input_cifer % 2
        duo_list.append(lost)
        input_cifer = main
        main = main // 2
        if input_cifer == 1:
            duo_list.append(input_cifer)
    
    duo_list = duo_list[::-1]

    print(duo_list)

from_dec_to_duo()
