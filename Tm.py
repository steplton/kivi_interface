from kivymd.app import MDApp
from kivymd.uix.dialog import MDDialog
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.pickers import MDDatePicker
from kivymd.uix.list import TwoLineAvatarListItem
from kivymd.uix.label import MDIcon
from kivymd.uix.selectioncontrol import MDCheckbox
from kivy.core.window import Window
Window.size = (660,560)

from datetime import datetime

# Class to create a marked list of tasks

class ListItemWithCheckbox(TwoLineAvatarListItem):
    def __init__(self, pk = None,  **kwargs):
        super().__init__(**kwargs)
        self.pk = pk
        #self.left_widget = LeftCheckBox()


    def mark_item(self, check, the_list_item):
        if check.active == True:
            the_list_item.text = '[s]' + the_list_item.text + '[/s]'
        else:
            pass

    def delete_item(self, the_list_item):
        self.parent.remove_widget(the_list_item)

# CheckBox class
class LeftCheckBox(MDIcon(), MDCheckbox):
    pass

# Class to create input dialog after button was pressed
class DialogContent(MDBoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.ids.date_text.text = datetime.now().strftime("%A %d %B %Y")

    # Method to show calendar after hitting the calendar button
    def show_date_picker(self):
        date_picker = MDDatePicker()
        date_picker.bind(on_save = self.on_save)
        date_picker.open()

    # Method to save current date into date label
    def on_save(self, instance, value, date_range):
        date = value.strftime("%A %d %B %Y")
        self.ids.date_text.text = str(date)


# Main App class
class TmApp(MDApp):
    task_list_dialog = None

    def build(self):
        # Setting up whole color theme
        self.theme_cls.theme_style = ("Light")
        self.theme_cls.primary_palette = ("Green")
        
    # Method refers to dialog class above
    def show_task_dialog(self):
        if not self.task_list_dialog:
            self.task_list_dialog = MDDialog(
                title = "Create task",
                type = "custom",
                content_cls = DialogContent()
            )
            self.task_list_dialog.open()
    
    # Method to close dialog after hitting cancel button
    def close_dialog(self, *args):
        self.task_list_dialog.dismiss()
        self.task_list_dialog = None

    # Method to add a task into task list class
    def add_task(self, task,  task_date):
        self.root.ids[''].add_widget(ListItemWithCheckbox(text = task.text,secondary_text = task_date))
        task.text = ''

if __name__ == '__main__':
    TmApp().run()
